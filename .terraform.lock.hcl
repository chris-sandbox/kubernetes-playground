# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.3.0"
  constraints = "2.3.0"
  hashes = [
    "h1:Kmcj3ajzt/lSQkbQwcjzUNK2RXXcHNDCs44LfDhZnaM=",
    "zh:1c0f68715cf0b84ab40ab08aa59232037325cffc2896ba109cae73c81ab021e9",
    "zh:306599aec6637c92349abb069d8fea3ebac58f52f61707956320a405f57e4a84",
    "zh:31db532f05e55cb52d61c12c10197dca48dc8809a4f9cc4a935d3161546968ca",
    "zh:3dba438c0167e5dcf09115f8d2c33c0a821e6b27e83ec6ccaac5fcb557a50bbb",
    "zh:770c906ab3eeb5c24c5b8bbcca3b18f137d5ac817bd73fa5c9146eb4a9d891d6",
    "zh:9221f2d275c776382234882d534a1147db04a8be490c023eb08c9a1e579db021",
    "zh:a4e25e5dd2ad06de6c7148a270b1178b6298846405ce66b9b4ca51ea35b66907",
    "zh:b3c5555e0c55efaa91de245e6d69e7140665554d2365db2f664802a36b59e0a8",
    "zh:c510655b6c5de0227babba5a8bb66a8c3d92af94e080ec1c39bde9509a2aa1a6",
    "zh:d04a135d9bf32c1a55abaaeb719903f4f67797434dd6d9f3219245f62a9a66be",
    "zh:dd5b99bec9425eb670be5d19b17336d0fa9b894649dac77eac532e4c626616f5",
    "zh:e57614fb9f3fbf774a9258a197840f40d0f343e8183eef7a842286a87cfc48d7",
    "zh:fee52e736edc5ef4088cedae6507790f35e4ee8a078bff1ef894a51dd65d058d",
  ]
}

provider "registry.terraform.io/kbst/kustomization" {
  version     = "0.3.0"
  constraints = ">= 0.2.0"
  hashes = [
    "h1:96Wn4L+AVhsdE0WAjC21Ni+Sq8xgO+HvH81Jv3Qdv28=",
    "zh:0ce39b23372cc11bc1234af2f54adb93b281672bd3fe9a441f35f30fe1d1c2d1",
    "zh:1f20808dcaf8ff6e9fde7f196844e593164e5b8b9ba9136e6aded78a3dca3fd1",
    "zh:25ad45be4d4c1b60f5898939b6d157f47b01981a916ca325008f6269f2bd622d",
    "zh:4822ff942f583e1e1c859050ebfcf52d44afb0a60654c7f1d8e06ecbe9a20032",
    "zh:7b05065ce53d9e0e90caade4520dfa57fca7736258aae7f86ebeaf40af87c35c",
    "zh:80a4079cb31f3d97c8ffe28dc6dccae0f1b549d001aed8cf8893ea1ee419b0f2",
    "zh:8eecd4bfe464368d71bf15da19ab7e9251eed0eab08380ba15bb14bbc5bb3059",
    "zh:9bed00852fbc9b7b9afb3cf63157ad22320e5ecd9a87c77d7e6fb140ef4acf3d",
    "zh:ae615c9826740d4a445b1c8eb6ce0b2fb85921f0c3534f9cfbb3828b4b1c8e2e",
    "zh:cc60ecf70458e6e0713df0ea3c1a0b717c5b9657ce9d4051f6a6fcec71d8fbf0",
    "zh:fdbbfba3eec718a72e0f844ebea17ecd487fd7e57b29522e1d8bf461d9c67887",
  ]
}
