# A project to play around with Kubernetes tooling

## Getting started 

To setup this repository, first clone it.

Then locally, run `terraform init`. This will setup all of the terraform providers.

## API Keys

Next, you need two API keys from DigitalOcean. 

One for the API, and one for the spaces API. 

The API key is used to create resources, and the spaces API key is used to store your terraform state securely.

### DigitalOcean API Key

Go to your DigitalOcean dashboard, and head over to `API`

Under **Personal Access Tokens**, click *Generate New Token*

For the token name, enter something which will remind you why this API token exists in 6 months.

Right after submitting your chosen name, you will be presented with a new API Token. 

Make sure you copy this token, as it will not be available for you to select again.

### DigitalOcean Spaces API Key

Go to your DigitalOcean dashboard, and head over to `API`

Under **Spaces access keys**, click *Generate New Key*

For the token name, enter something which will remind you why this API key exists in 6 months.

Right after submitting your chosen name, you will be presented with a new API Key and a Secret right below it.

Make sure you copy these, as they will not be available for you to select again.

### DigitalOcean Space

We are going to use a DigitalOcean space to store our terraform file. 

This is done to keep our state together in the same Cloud. 

You are welcome to use S3, and the instruction will remain the same, except for the bucket creation. 

You can find more details [here](https://www.terraform.io/docs/backends/types/s3.html)

For the DigitalOcean version, I am using the setup mentioned [here](https://chris-vermeulen.com/don-t-push-your-terraform-state-files/)

Create a new space, and name it something useful for your project. This is one of the only manual steps in the entire process.

## Backend Setup

For this project, I have decided to use a Terraform backend. 

You'll need to specify a `backend.conf` file with your own private bucket details in it.

The format of that file looks like this
```.env
bucket = "[YOUR_DO_SPACE_NAME]"
key    = "[STATE_FILE_KEY]"
access_key = "[DO_SPACES_KEY]"
secret_key = "[DO_SPACES_SECRET]"
endpoint = "https://ams3.digitaloceanspaces.com"
# DO uses the S3 format
# eu-west-1 is used to pass TF validation
# Region is ACTUALLY ams3 on DO
region = "eu-west-1"
# Deactivate a few checks as TF will attempt these against AWS
skip_credentials_validation = true
skip_metadata_api_check = true
```

Fill in your detail, and pass it into your `terraform init` command using the `--backend-config=PATH` flag

## Current tools implemented