terraform {
  required_providers {
    kustomization = {
      source  = "kbst/kustomization"
      version = ">= 0.2"
    }
  }
  required_version = ">= 0.12"
}

provider "kustomization" {}

data "kustomization_build" "appliance-monitoring" {
  path = "${abspath(path.module)}/manifests"
}

resource "kustomization_resource" "appliance-monitoring" {
  for_each = data.kustomization_build.appliance-monitoring.ids

  manifest = data.kustomization_build.appliance-monitoring.manifests[each.value]
}
