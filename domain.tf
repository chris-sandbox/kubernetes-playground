# Add an A record to the domain for www.example.com.
resource "digitalocean_record" "www" {
  domain = "cmsoft.co.za"
  type   = "A"
  name   = "*.sandbox"
  value  = "138.68.112.33"
}