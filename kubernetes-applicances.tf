module "nginx-ingress" {
  source = "git::https://gitlab.com/chris-sandbox/terraform-nginx-ingress-kubernetes.git"
}

module "sealed-secrets" {
  source = "git::https://gitlab.com/chris-sandbox/terraform-sealed-secrets-kubernetes.git"
}

module "harbor" {
  source = "./_local_harbor"
}

module "prometheus-operator" {
  source = "./_local_prometheus-operator"
}

module "node-exporter" {
  source = "./_local_node-exporter"
}
