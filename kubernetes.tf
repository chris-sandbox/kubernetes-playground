resource "digitalocean_kubernetes_cluster" "kubernetes-playground" {
  name = "kubernetes-playground"
  node_pool {
    name = "default"
    node_count = 2
    size = "s-4vcpu-8gb"
    labels = {
      environment = "dev"
    }
  }
  region = var.do_region
  version = "1.19.3-do.3"
  tags = [
    "KubernetesToolingExploration"
  ]
}

resource "digitalocean_project_resources" "harbor-kubernetes-project-link" {
  project = digitalocean_project.kubernetes-sandbox.id
  resources = ["do:kubernetes:${digitalocean_kubernetes_cluster.kubernetes-playground.id}"]
}
