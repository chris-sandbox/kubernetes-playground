resource "digitalocean_database_cluster" "kubernetes-playground" {
  engine = "pg"
  name = "kubernetes-playground"
  node_count = 1
  region = var.do_region
  size = "db-s-1vcpu-1gb"
  version = "12"
}

resource "digitalocean_database_db" "clair" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "clair"
}

resource "digitalocean_database_user" "clair" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "clair"
}

resource "digitalocean_database_db" "core" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "core"
}

resource "digitalocean_database_user" "core" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "core"
}

resource "digitalocean_database_db" "notary-server" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "notary-server"
}

resource "digitalocean_database_user" "notary-server" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "notary-server"
}

resource "digitalocean_database_db" "notary-signer" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "notary-signer"
}

resource "digitalocean_database_user" "notary-signer" {
  cluster_id = digitalocean_database_cluster.kubernetes-playground.id
  name       = "notary-signer"
}