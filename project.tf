resource "digitalocean_project" "kubernetes-sandbox" {
  name        = "Kubernetes Sandbox"
  description = "A playground to test out some tooling"
  purpose     = "Exploration"
  environment = "Development"
}